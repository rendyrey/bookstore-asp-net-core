﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BookStore.Models;

namespace BookStore.Controllers
{
    public class AuthorContactsController : Controller
    {
        private readonly BookStoreContext _context;

        public AuthorContactsController(BookStoreContext context)
        {
            _context = context;
        }

        // GET: AuthorContacts
        public async Task<IActionResult> Index()
        {
            var bookStoreContext = _context.AuthorContact.Include(a => a.Author);
            return View(await bookStoreContext.ToListAsync());
        }

        // GET: AuthorContacts/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var authorContact = await _context.AuthorContact
                .Include(a => a.Author)
                .FirstOrDefaultAsync(m => m.AuthorId == id);
            if (authorContact == null)
            {
                return NotFound();
            }

            return View(authorContact);
        }

        // GET: AuthorContacts/Create
        public IActionResult Create()
        {
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "Name");
            return View();
        }

        // POST: AuthorContacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AuthorId,ContactNumber,Address")] AuthorContact authorContact)
        {
            if (ModelState.IsValid)
            {
                _context.Add(authorContact);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "Name", authorContact.AuthorId);
            return View(authorContact);
        }

        // GET: AuthorContacts/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var authorContact = await _context.AuthorContact.FindAsync(id);
            if (authorContact == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "Name", authorContact.AuthorId);
            return View(authorContact);
        }

        // POST: AuthorContacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("AuthorId,ContactNumber,Address")] AuthorContact authorContact)
        {
            if (id != authorContact.AuthorId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(authorContact);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AuthorContactExists(authorContact.AuthorId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Author, "Id", "Name", authorContact.AuthorId);
            return View(authorContact);
        }

        // GET: AuthorContacts/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var authorContact = await _context.AuthorContact
                .Include(a => a.Author)
                .FirstOrDefaultAsync(m => m.AuthorId == id);
            if (authorContact == null)
            {
                return NotFound();
            }

            return View(authorContact);
        }

        // POST: AuthorContacts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var authorContact = await _context.AuthorContact.FindAsync(id);
            _context.AuthorContact.Remove(authorContact);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AuthorContactExists(long id)
        {
            return _context.AuthorContact.Any(e => e.AuthorId == id);
        }
    }
}
